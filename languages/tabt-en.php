<?php 
$langLabels = array(
	'Seasons_PageTitle' => 'Seasons', 
	'Seasons_CurrentSeasonName' => 'Current Season', 
	'Seasons_Season' => 'Season', 
	'Seasons_Name' => 'Name', 
	'Seasons_IsCurrent' => 'Is Current',
	'Seasons_Actions' => 'Actions',

	'Clubs_PageTitle' => 'Clubs', 
	'Clubs_ClubCount' => 'Clubs Count', 
	'Clubs_UniqueIndex' => 'Unique Index', 
	'Clubs_Name' => 'Name', 
	'Clubs_LongName' => 'Long Name', 
	'Clubs_Category' => 'Category', 
	'Clubs_CategoryName' => 'Category Name', 
	'Clubs_Actions' => 'Actions',

	'ClubDetails_PageTitle' => 'Club Details', 

	'Members_PageTitle' => 'Members', 
	'Members_MemberCount' => 'Members Count', 
	'Members_Position' => 'Position', 
	'Members_UniqueIndex' => 'Unique Index', 
	'Members_RankingIndex' => 'Ranking Index', 
	'Members_FirstName' => 'First Name', 
	'Members_LastName' => 'Last Name', 
	'Members_Ranking' => 'Ranking', 
	'Members_Actions' => 'Actions',

	'MemberDetails_PageTitle' => 'Member Details', 
	'MemberDetails_Position' => 'Position', 
	'MemberDetails_UniqueIndex' => 'Unique Index', 
	'MemberDetails_FirstName' => 'First Name', 
	'MemberDetails_LastName' => 'Last Name', 
	'MemberDetails_Ranking' => 'Ranking', 
	'MemberDetails_Status' => 'Status', 
	'MemberDetails_Club' => 'Club', 
	'MemberDetails_Gender' => 'Gender', 
	'MemberDetails_Category' => 'Category', 
	'MemberDetails_BirthDate' => 'BirthDate', 
	'MemberDetails_Email' => 'Email', 
	
	'Matches_PageTitle' => 'Matches', 
	'Matches_MatchCount' => 'Match Count', 
	'Matches_DivisionName' => 'Division Name', 
	'Matches_MatchId' => 'Match Id', 
	'Matches_WeekName' => 'Week Name', 
	'Matches_Date' => 'Date', 
	'Matches_Time' => 'Time', 
	'Matches_Venue' => 'Venue', 
	'Matches_HomeClub' => 'Home Club', 
	'Matches_HomeTeam' => 'Home Team', 
	'Matches_AwayClub' => 'Away Club', 
	'Matches_AwayTeam' => 'Away Team', 
	'Matches_Score' => 'Score', 
	'Matches_MatchUniqueId' => 'Match Unique Id', 
	'Matches_NextWeekName' => 'Next Week Name', 
	'Matches_PreviousWeekName' => 'Previous Week Name', 
	'Matches_IsHomeForfeited' => 'Is Home Forfeited', 
	'Matches_IsAwayForfeited' => 'Is Away Forfeited', 
	'Matches_MatchDetails' => 'Match Details', 
	'Matches_Actions' => 'Actions',

	'MatchDetails_PageTitle' => '', 

	'ClubTeams_PageTitle' => 'Club Teams', 
	'ClubTeams_ClubName' => 'Club Name', 
	'ClubTeams_TeamCount' => 'Team Count', 
	'ClubTeams_TeamEntries' => 'Team Entries', 
	'ClubTeams_TeamId' => 'TeamId', 
	'ClubTeams_Team' => 'Team', 
	'ClubTeams_DivisionId' => 'Division Id', 
	'ClubTeams_DivisionName' => 'Division Name', 
	'ClubTeams_DivisionCategory' => 'Division Category', 
	'ClubTeams_MatchType' => 'Match Type', 
	'ClubTeams_Actions' => 'Actions',

	'DivisionRanking_PageTitle' => 'Division Ranking', 
	'DivisionRanking_DivisionName' => 'Division Name', 
	'DivisionRanking_Position' => 'Position', 
	'DivisionRanking_Team' => 'Team', 
	'DivisionRanking_GamesPlayed' => 'GP', 
	'DivisionRanking_GamesWon' => 'GW', 
	'DivisionRanking_GamesLost' => 'GL', 
	'DivisionRanking_GamesDraw' => 'GD', 
	'DivisionRanking_IndividualMatchesWon' => 'MW', 
	'DivisionRanking_IndividualMatchesLost' => 'ML', 
	'DivisionRanking_IndividualSetsWon' => 'SW', 
	'DivisionRanking_IndividualSetsLost' => 'SL', 
	'DivisionRanking_Points' => 'Points', 
	'DivisionRanking_TeamClub' => 'Team Club', 
	'DivisionRanking_Footer' => '
		<p class="tabtNote">Note about possible wrong data in table because not added individual data for all matches</p>
		<p class="tabtLegend">Legend:</p>
		<ul class="tabtLegend">
			<li>GP - Games Played</li>
			<li>GW - Games Won</li>
			<li>GL - Games Lost</li>
			<li>GD - Games Draw</li>
			<li>MW - Individual Matches Won</li>
			<li>ML - Individual Matches Lost</li>
			<li>SP - Individual Sets Won</li>
			<li>SL - Individual Sets Lost</li>
		</ul>
		', 

	'Divisions_PageTitle' => 'Divisions',
	'Divisions_DivisionCount' => 'Divisions Count',
	'Divisions_DivisionId' => 'Id',
	'Divisions_DivisionName' => 'Name',
	'Divisions_DivisionCategory' =>' Category',
	'Divisions_Level' => 'Level',
	'Divisions_MatchType' => 'Match Type',
	'Divisions_Actions' => 'Actions',

	'Tournaments_Columns' => 'Tournaments Columns', 

	'MatchSystems_Columns' => 'Match Systems Columns', 
	
	'lblViewClubs' => 'Clubs',
	'lblViewDivisions' => 'Divisions',
	'lblViewMembers' => 'Members',
	'lblViewDetails' => 'Details',
	'lblViewDivisionRanking' => 'Ranking',
	'lblViewMatches' => 'Matches',
	'lblViewMatchDetails' => 'Details',
);