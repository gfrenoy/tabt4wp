<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 *
 * @package    Tabt4wp
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
  exit;
}
