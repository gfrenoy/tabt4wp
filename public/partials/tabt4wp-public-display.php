<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 *
 * @package    Tabt4wp
 * @subpackage Tabt4wp/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
