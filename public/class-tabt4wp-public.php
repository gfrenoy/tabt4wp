<?php
/**
 *  This file is part of TabT4wp
 *
 *  Copyright (C) 2017, Ljupco Prchkovski
 *  Copyright (C) 2017, frenoy.net sprl (info@frenoy.net)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 * @package    Tabt4wp
 * @subpackage Tabt4wp/public
 * @author     Ljupcho Prchkovski <ljupcho.prchkovski@gmail.com>
 */
class Tabt4wp_Public {
  private $plugin_name;
  private $version;

  private $params;
  private $server_url;
  private $server_account;
  private $server_password;
  private $creds;
  private $lbl;
  private $columns;

  /**
  * Initialize the class and set its properties.
  *
  * @since    1.0.0
  * @param      string    $plugin_name       The name of the plugin.
  * @param      string    $version    The version of this plugin.
  */
  public function __construct( $plugin_name, $version, $labels ) {
    $this->plugin_name = $plugin_name;
    $this->version = $version;
    $this->lbl = $labels;
  }

  /**
  * Register the stylesheets for the public-facing side of the site.
  *
  * @since    1.0.0
  */
  public function enqueue_styles() {
    wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tabt4wp-public.css', array(), $this->version, 'all' );
  }

  /**
  * Register the JavaScript for the public-facing side of the site.
  *
  * @since    1.0.0
  */
  public function enqueue_scripts() {
    wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tabt4wp-public.js', array('jquery' ), $this->version, false );
  }

  public function dynamic_meta_title() {
    global $post;
    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'TabT') )
    {
      if(isset($_GET['get']))
      {
        $post->post_title = '';
      }
    }
    if(isset($_GET['get'])){
      return $_GET['get'];
    }
  }

  public function tabt($parameters, $content = "") {
    $this->params = array_merge($parameters, array_change_key_case($_GET));
    $this->tabt_url = get_option( 'tabt4wp_tabt_url' );
    $this->server_url = get_option( 'tabt4wp_server_url' );
    $this->server_account = get_option( 'tabt4wp_server_account' );
    $this->server_password = get_option( 'tabt4wp_server_password' );
    $this->creds = array ('Account' => $this->server_account,'Password'=> $this->server_password );
    $this->sepparator = get_option('tabt4wp_sepparator');

    if(trim($this->sepparator)=='') {
      $this->sepparator = ' : ';
    } else {
      $this->sepparator = ' ' . $this->sepparator . ' ';
    }

    if (!isset($this->params['get'])) {
      return 'Please check and correct your shortcode';
    }

    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    require_once( ABSPATH . 'wp-admin/includes/screen.php' );
    require_once( ABSPATH . 'wp-admin/includes/class-wp-screen.php' );
    require_once( ABSPATH . 'wp-admin/includes/template.php' );

    ob_start();
    //hide title on page where whortcode is used
    if(isset($_GET['get']))
    {
      if(isset($this->lbl[$this->params['get'] . '_PageTitle']) && $this->lbl[$this->params['get'] . '_PageTitle']!='')
      {
        echo '<header class="entry-header"><h1 class="entry-title">' . $this->lbl[$this->params['get'] . '_PageTitle'] . '</h1></header>';
      }
    }
    //build content displayed before table
    switch ($this->params['get']) {
      case 'Seasons':
        $data = $this->getData('Seasons');
        $data = $this->prepareSeasonsData($data);
        break;
      case 'Clubs':
        $data = $this->getData('Clubs');
        $data = $this->prepareClubsData($data);
        break;
      case 'Divisions':
        $data = $this->getData('Divisions');
        $data = $this->prepareDivisionsData($data);
        break;
      case 'DivisionRanking':
        $data = $this->getData('DivisionRanking');
        $data = $this->prepareDivisionRankingData($data);
        break;
      case 'Members':
        $data = $this->getData('Members');
        $data = $this->prepareMembersData($data);
        break;
      case 'Matches':
        $data = $this->getData('Matches');
        $data = $this->prepareMatchesData($data);
        break;
      case 'ClubDetails':
        $data = $this->prepareClubDetailsData();
        break;
      case 'MatchDetails':
        $data = $this->prepareMatchDetailsData();
        break;
      case 'MemberDetails':
        $this->params['extendedinformation'] = 'true';
        $this->params['withresults'] = 'true';
        $data = $this->getData('Members');
        $data = $this->prepareMemberDetailsData($data);
        break;
      default:
        return 'Unknown table given in "Get" parameter, please check and correct your shortcode.';
    }
    return ob_get_clean();
  }

  private function getColumns($table) {
    global $wpdb;

    $querycolumns = "SELECT $wpdb->options.* FROM $wpdb->options WHERE $wpdb->options.option_name = 'tabt4wp_table_" . $table . "'";
    $results = $wpdb->get_results($querycolumns, OBJECT);
    $this->columns = maybe_unserialize( $results[0]->option_value );
  }

  private function getData($function)
  {
    $datacacheperiod = (int)str_replace('cache','',get_option( 'tabt4wp_data_cache' )) * 60 * 60;
    $cachekey = 'TabT_' . http_build_query($this->params);
    $cache = false;
    $data = array();

    if ($datacacheperiod != 0) {
      $cache = get_transient($cachekey);
    }
    if ($cache == false || isset($_GET['nocache'])) {
      $soap_params = array ('Credentials' => $this->creds);
      foreach($this->getFunctionAttributes($function) as $attribute)
      {
        if(isset($this->params[strtolower($attribute)])){
          $soap_params[$attribute] = $this->params[strtolower($attribute)];
        }
      }
      $soap = new SoapClient ( $this->server_url, array ('trace'=> true ) );
      $strfunction = 'Get' . $function;
      $data = $soap->{$strfunction}($soap_params);

      if ($datacacheperiod != 0) {
        set_transient($cachekey, $data, $datacacheperiod );
      }
    } else {
      $data = maybe_unserialize( $cache );
    }

    return json_decode(json_encode($data), true);
  }

  private function printTable($data)
  {
    $myListTable = new TabT_List_Table();
    $myListTable->set_order();
    $myListTable->set_orderby();
    $myListTable->prepare_tabledata($data);
    $myListTable->prepare_items();
    $myListTable->display();
  }

  private function printContent($data, $class='', $title='')
  {
    echo '<div class="tabtContent ' . $class . '">';
    echo $title;
        foreach($data as $line){
      echo '<p class="tabtHeaderLine"><span>' . $line['label'] . ':</span> ' . $line['value'] . '</p>';
    }
    echo '</div>';
  }

  private function prepareSeasonsData($data)
  {
    $this->getColumns('Seasons');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
      $prop = str_replace('Seasons_','',$column);
      if ($prop == 'CurrentSeason') {
        //do not use
      } elseif ($prop == 'CurrentSeasonName') {
        $arrContent[] = array('label' => $this->lbl[$column],'value' => $data[$prop]);
      } else {
        $columns[$prop] = $this->lbl[$column];
      }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['SeasonEntries'][0]))
    {
      $resultarray = $data['SeasonEntries'];
    } else {
      $resultarray[] = $data['SeasonEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++)
    {
      //Actions content
      $arrQuery = array ( 'get'=>'Clubs', 'Season'=>$resultarray[$i]['Season']);
      $resultarray[$i]['Actions'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewClubs'] . '</a>';
      $arrQuery = array ( 'get'=>'Divisions', 'Season'=>$resultarray[$i]['Season']);
      $resultarray[$i]['Actions'] .= ' | <a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewDivisions'] . '</a>';
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareDivisionsData($data)
  {
    $this->getColumns('Divisions');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
        $prop = str_replace('Divisions_','',$column);
        if ($prop == 'DivisionCount') {
            $arrContent[] = array('label' => $this->lbl[$column],'value' => $data[$prop]);
        } else {
            $columns[$prop] = $this->lbl[$column];
        }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['DivisionEntries'][0])) {
        $resultarray = $data['DivisionEntries'];
    } else {
        $resultarray[] = $data['DivisionEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++){
      //Actions content
      $arrQuery = array ( 'get'=>'DivisionRanking', 'Season'=>$this->params['season'], 'DivisionId'=>$resultarray[$i]['DivisionId']);
      $resultarray[$i]['Actions'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewDivisionRanking'] . '</a>';
      $arrQuery = array ( 'get'=>'Matches', 'Season'=>$this->params['season'], 'DivisionId'=>$resultarray[$i]['DivisionId'], 'ShowDivisionName'=>'yes', 'WithDetails'=>'false', 'YearDateFrom'=>'200-01-01', 'YearDateTo'=>'2020-12-31');
      $resultarray[$i]['Actions'] .= ' | <a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewMatches'] . '</a>';
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareDivisionRankingData($data)
  {
    $this->getColumns('DivisionRanking');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
        $prop = str_replace('DivisionRanking_','',$column);
        if ($prop == 'DivisionName') {
            $arrContent[] = array('label' => $this->lbl[$column],'value' => $data[$prop]);
        } else {
            $columns[$prop] = $this->lbl[$column];
        }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['RankingEntries'][0])) {
        $resultarray = $data['RankingEntries'];
    } else {
        $resultarray[] = $data['RankingEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++){
      //Team field link
      $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['TeamClub']);
      $resultarray[$i]['Team'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $resultarray[$i]['Team'] . '</a>';
    }

    if(isset($this->params['club'])) {
      foreach($resultarray as $k => $v) {
        if ($v['TeamClub'] != $this->params['club']) {
          unset($resultarray [$k]);
        }
      }
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
    if(!isset($this->params['club'])){
      echo '<div class="tabtContent tabtFooter">' . $this->lbl['DivisionRanking_Footer'] . '</div>';
    }
  }

  private function prepareClubsData($data)
  {
    $this->getColumns('Clubs');

    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
      $prop = str_replace('Clubs_','',$column);
      if ($prop == 'ClubCount') {
        $arrContent[] = array('label' => $this->lbl[$column], 'value' => $data[$prop]);
      } else {
        $columns[$prop] = $this->lbl[$column];
      }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['ClubEntries'][0])) {
      $resultarray = $data['ClubEntries'];
    } else {
      $resultarray[] = $data['ClubEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++) {
      // Actions content
      $arrQuery = array ( 'get'=>'Members', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['UniqueIndex']);
      $resultarray[$i]['Actions'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewMembers'] . '</a>';

      $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['UniqueIndex']);
      $resultarray[$i]['Actions'] .= ' | <a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewDetails'] . '</a>';
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareClubDetailsData()
  {
    $this->params['get'] = 'Clubs';
    $dataClubs = $this->getData('Clubs');
    $this->params['get'] = 'ClubTeams';
    $dataClubTeams = $this->getData('ClubTeams');

    $resultarrayClubTeams = array();
    if (isset($dataClubTeams['TeamEntries'][0])) {
        $resultarrayClubTeams = $dataClubTeams['TeamEntries'];
    } else {
        $resultarrayClubTeams[] = $dataClubTeams['TeamEntries'];
    }

    echo '<h2>Name: ' . $dataClubs['ClubEntries']['Name'] . '</h2>';

    echo '<div class="tabtTab tabtHeader">';
    echo '<div class="tabtTabName tabActive" data-target="tabMembers">Members</div>';
    //~ for($i = 0; $i < count($resultarrayClubTeams); $i++){
      //~ echo '<div class="tabtTabName" data-target="Team' . ($i + 1) . '">Team ' . ($i + 1) . '</div>';
    //~ }
    //~ if($dataClubs['ClubEntries']['VenueCount']!='0'){
      //~ echo '<div class="tabtTabName" data-target="tabVenues">Venues</div>';
    //~ }
    echo '</div>';

    echo '<div class="tabtTab tabtTabContent tabMembers tabActive">';
    $this->params['get'] = 'Members';
    $data = $this->getData('Members');
    $data = $this->prepareMembersData($data);
    echo '</div>';

    //~ $strCurrentPageLink = get_page_link();
    //~ for($i = 0; $i < count($resultarrayClubTeams); $i++)
    //~ {
      //~ echo '<div class="tabtTab tabtTabContent Team' . ($i + 1) . '">';
      //~ echo '<h3>Ranking</h3>';
      //~ $this->params['divisionid'] = $resultarrayClubTeams[$i]['DivisionId'];
      //~ $this->params['showdivisionname'] = 'no';
      //~ $this->params['withdetails'] = 'false';
      //~ $this->params['yeardatefrom'] = '2000-01-01';
      //~ $this->params['yeardateto'] = '2023-12-31';
      //~ $this->params['get'] = 'DivisionRanking';
      //~ $data = $this->getData('DivisionRanking');
      //~ $data = $this->prepareDivisionRankingData($data);
      //~ echo '<p>&nbsp;</p>';
      //~ echo '<h3>Matches</h3>';
      //~ $this->params['divisionid'] = $resultarrayClubTeams[$i]['DivisionId'];
      //~ $this->params['showdivisionname'] = 'no';
      //~ $this->params['withdetails'] = 'false';
      //~ $this->params['yeardatefrom'] = '2000-01-01';
      //~ $this->params['yeardateto'] = '2023-12-31';
      //~ $this->params['get'] = 'Matches';
      //~ $data = $this->getData('Matches');
      //~ $data = $this->prepareMatchesData($data);
      //~ echo '</div>';
    //~ }

    //~ if($dataClubs['ClubEntries']['VenueCount']!='0')
    //~ {
      //~ echo '<div class="tabtTab tabtTabContent tabVenues">';

      //~ $venuesarray = array();
      //~ if (isset($dataClubs['ClubEntries']['VenueEntries'][0]))
      //~ {
        //~ $venuesarray = $dataClubs['ClubEntries']['VenueEntries'];
      //~ } else {
        //~ $venuesarray[] = $dataClubs['ClubEntries']['VenueEntries'];
      //~ }
      //~ for($y = 0; $y < count($venuesarray); $y++)
      //~ {
        //~ echo '<h3>' . $venuesarray[$y]['Name'] . '</h3>';
        //~ if(isset($venuesarray[$y]['Phone']) && trim($venuesarray[$y]['Phone']) != ''){ echo '<p>Phone: ' . $venuesarray[$y]['Phone'] . '</p>'; }
        //~ if(isset($venuesarray[$y]['Street']) && trim($venuesarray[$y]['Street']) != ''){ echo '<p>Street: ' . $venuesarray[$y]['Street'] . '</p>'; }
        //~ if(isset($venuesarray[$y]['Town']) && trim($venuesarray[$y]['Town']) != ''){ echo '<p>Town: ' . $venuesarray[$y]['Town'] . '</p>'; }
        //~ if(isset($venuesarray[$y]['Comment']) && trim($venuesarray[$y]['Comment']) != ''){ echo '<p>Comment: ' . $venuesarray[$y]['Comment'] . '</p>'; }
      //~ }
      //~ echo '</div>';
    //~ }

    //$this->printContent($arrContent);
    //$this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareMembersData($data)
  {
    $this->getColumns('Members');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
      $prop = str_replace('Members_','',$column);
      if ($prop == 'MemberCount')
      {
        $arrContent[] = array('label' => $this->lbl[$column],'value' => $data[$prop]);
      } else {
        $columns[$prop] = $this->lbl[$column];
      }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['MemberEntries'][0]))
    {
      $resultarray = $data['MemberEntries'];
    } else {
      $resultarray[] = $data['MemberEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++){
      // Actions content
      $arrQuery = array ( 'result'=>'1', 'sel'=>$resultarray[$i]['UniqueIndex']);
      $resultarray[$i]['Actions'] = '<a href="' . $this->tabt_url . '/?' . http_build_query($arrQuery) . '" target="_blank">' . $this->lbl['lblViewDetails'] . '</a>';
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareMemberDetailsData($data)
  {
    $this->getColumns('MemberDetails');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
      $prop = str_replace('MemberDetails_','',$column);
      $arrContent[] = array('label' => $this->lbl[$column],'value' => $data['MemberEntries'][$prop]);
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['MemberEntries'][0]))
    {
      $resultarray = $data['MemberEntries'];
    } else {
      $resultarray[] = $data['MemberEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++){
      //Club field link
      $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['Club']);
      $resultarray[$i]['Club'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $resultarray[$i]['Club'] . '</a>';
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareMatchesData($data)
  {
    $this->getColumns('Matches');
    $arrContent = array();
    $columns = array();
    foreach ($this->columns as $column) {
      $prop = str_replace('Matches_','',$column);
      if ($prop == 'MatchCount')
      {
        $arrContent[] = array('label' => $this->lbl[$column],'value' => $data[$prop]);
      } elseif ($prop == 'DivisionName') {
        $arrContent['DivisionName'] = array('label' => $this->lbl[$column],'value' => '');
      } else {
        $columns[$prop] = $this->lbl[$column];
      }
    }

    $sortable = array();
    $hidden = array();

    $resultarray = array();
    if (isset($data['TeamMatchesEntries'][0]))
    {
      $resultarray = $data['TeamMatchesEntries'];
    } else {
      $resultarray[] = $data['TeamMatchesEntries'];
    }

    $strCurrentPageLink = get_page_link();
    for($i = 0; $i < count($resultarray); $i++)
    {
      //DivisionName before table
      if(isset($arrContent['DivisionName']['label']) && isset($resultarray[$i]['DivisionName']))
      {
        $arrContent['DivisionName']['value'] = $resultarray[$i]['DivisionName'];
      } else {
        unset($arrContent['DivisionName']);
      }

      //Columns content
      $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['HomeClub']);
      $resultarray[$i]['HomeTeam'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $resultarray[$i]['HomeTeam'] . '</a>';
      $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$resultarray[$i]['AwayClub']);
      $resultarray[$i]['AwayTeam'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $resultarray[$i]['AwayTeam'] . '</a>';

      $resultarray[$i]['Score'] = isset($resultarray[$i]['Score']) ? str_replace('-',$this->sepparator,$resultarray[$i]['Score']) : '';
      $resultarray[$i]['MatchUniqueId'] = isset($resultarray[$i]['MatchUniqueId']) ? $resultarray[$i]['MatchUniqueId'] : '';
      $resultarray[$i]['NextWeekName'] = isset($resultarray[$i]['NextWeekName']) ? $resultarray[$i]['NextWeekName'] : '';
      $resultarray[$i]['PreviousWeekName'] = isset($resultarray[$i]['PreviousWeekName']) ? $resultarray[$i]['PreviousWeekName'] : '';
      $resultarray[$i]['IsHomeForfeited'] = isset($resultarray[$i]['IsHomeForfeited']) ? $resultarray[$i]['IsHomeForfeited'] : '';
      $resultarray[$i]['IsAwayForfeited'] = isset($resultarray[$i]['IsAwayForfeited']) ? $resultarray[$i]['IsAwayForfeited'] : '';
      $resultarray[$i]['MatchDetails'] = '';

      //Actions content
      if($resultarray[$i]['Score'] != '')
      {
        $arrQuery = array ( 'get'=>'MatchDetails', 'Season'=>$this->params['season'], 'DivisionId'=>$this->params['divisionid'], 'ShowDivisionName'=>'yes', 'WithDetails'=>'yes', 'YearDateFrom'=>'200-01-01', 'YearDateTo'=>'2020-12-31', 'MatchUniqueId'=>$resultarray[$i]['MatchUniqueId']);
        $resultarray[$i]['Actions'] = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $this->lbl['lblViewMatchDetails'] . '</a>';
      } else {
        $resultarray[$i]['Actions'] = '';
      }
    }

    $this->printContent($arrContent);
    $this->printTable( array('data' => $resultarray, 'columns' => $columns, 'sortable' => $sortable, 'hidden' => $hidden) );
  }

  private function prepareMatchDetailsData()
  {
    $this->params['get'] = 'Matches';
    $data = $this->getData('Matches');
    $match = $data['TeamMatchesEntries'];
    $strCurrentPageLink = get_page_link();

    $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$match['HomeClub']);
    $HomeTeam = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $match['HomeTeam'] . '</a>';
    $arrQuery = array ( 'get'=>'ClubDetails', 'Season'=>$this->params['season'], 'Club'=>$match['AwayClub']);
    $AwayTeam = '<a href="' . $strCurrentPageLink . '?' . http_build_query($arrQuery) . '">' . $match['AwayTeam'] . '</a>';

    echo '<div class="tabtMatchDetails">';
      echo '<div class="MatchTeams"><span class="MatchTeamsName">' . $HomeTeam . '</span><span class="MatchTeamsSepparator">' . $this->sepparator . '</span><span class="MatchTeamsName">' . $AwayTeam . '</span></div>';
      $arrResult = explode('-',$match['Score']);
      echo '<div class="MatchScore"><span class="MatchScorePoints">' . $arrResult[0] . '</span><span class="MatchScoreSepparator">' . $this->sepparator . '</span><span class="MatchScorePoints">' . $arrResult[1] . '</span></div>';
      echo '<div class="MatchInfo">';
        echo '<p>' . $match['DivisionName'] . '</p>';
        echo '<p>' . $match['Date'] . ' ' . $match['Time'] . '</p>';
        echo '<p>Week: ' . $match['WeekName'] . '</p>';
    echo '</div>';

    if(!$match['MatchDetails']['DetailsCreated'])
    {
      return;
    } else {
      $columns = array (
        'Player' => 'Player',
        'Position' => 'Position',
        'Ranking' => 'Ranking',
        'VictoryCount' => 'Victory',
      );

      $dataHomePlayers = array();
      $dataAwayPlayers = array();
      foreach($match['MatchDetails']['HomePlayers']['Players'] as $k=>$v){
        $dataHomePlayers[$v['UniqueIndex']] = array (
          'Player' => $v['FirstName'] . ' ' . $v['LastName'],
          'Position' => $v['Position'],
          'Ranking' => $v['Ranking'],
          'VictoryCount' => $v['VictoryCount'],
        );
      }
      foreach($match['MatchDetails']['AwayPlayers']['Players'] as $k=>$v){
        $dataAwayPlayers[$v['UniqueIndex']] = array (
          'Player' => $v['FirstName'] . ' ' . $v['LastName'],
          'Position' => $v['Position'],
          'Ranking' => $v['Ranking'],
          'VictoryCount' => $v['VictoryCount'],
        );
      }

      echo '<div class="tabtMatchPlayers">';
        echo '<div style="float:left;">';
          echo '<h5>Home Team Players</h5>';
              $this->printTable( array('data' => $dataHomePlayers, 'columns' => $columns, 'sortable' => array(), 'hidden' => array()) );
        echo '</div>';
        echo '<div style="float:right;">';
          echo '<h5>Away Team Players</h5>';
              $this->printTable( array('data' => $dataAwayPlayers, 'columns' => $columns, 'sortable' => array(), 'hidden' => array()) );
        echo '</div>';
      echo '</div>';


      $columns = array (
        'Match' => 'Match',
        'HomePlayer' => 'Home Player',
        'AwayPlayer' => 'Away Player',
        'Result' => 'Result',
      );
      $dataIndividual = array();
      foreach($match['MatchDetails']['IndividualMatchResults'] as $k=>$v){
        if(isset($v['HomeSetCount'])){
          $HomePlayer = $dataHomePlayers[$v['HomePlayerUniqueIndex']]['Player'];
          $AwayPlayer = $dataAwayPlayers[$v['AwayPlayerUniqueIndex']]['Player'];
          if((int)$v['HomeSetCount'] > (int)$v['AwaySetCount']){
            $HomePlayer = '<b>' . $HomePlayer . '</b>';
          } else {
            $AwayPlayer = '<b>' . $AwayPlayer . '</b>';
          }
          $dataIndividual[$v['Position']] = array (
            'Match' => $v['Position'],
            'HomePlayer' => $HomePlayer,
            'AwayPlayer' => $AwayPlayer,
            'Result' => $v['HomeSetCount'] . $this->sepparator . $v['AwaySetCount'],
          );
        }
      }
      echo '<div class="tabtIndividualMatchResults">';
        echo '<h5>Individual Match Results</h5>';
            $this->printTable( array('data' => $dataIndividual, 'columns' => $columns, 'sortable' => array(), 'hidden' => array()) );
      echo '</div>';
    }

    echo '</div>';
  }

  private function getFunctionAttributes($function)
  {
    switch($function){
      case 'Clubs':
        return array('Season', 'ClubCategory', 'Club');
        break;

      case 'ClubTeams':
        return array('Season', 'Club');
        break;

      case 'Divisions':
        return array('Season', 'Level', 'ShowDivisionName');
        break;

      case 'DivisionRanking':
        return array('DivisionId', 'WeekName', 'RankingSystem');
        break;

      case 'Matches':
        return array('DivisionId', 'Club', 'Team', 'DivisionCategory', 'Season', 'WeekName', 'Level', 'ShowDivisionName', 'YearDateFrom', 'YearDateTo', 'WithDetails', 'MatchId', 'MatchUniqueId');
        break;

      case 'Members':
        return array('Club', 'Season', 'PlayerCategory', 'UniqueIndex', 'NameSearch', 'ExtendedInformation', 'WithResults');
        break;

      default:
        return array();
        break;
    }
  }

}
