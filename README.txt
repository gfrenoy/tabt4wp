=== Plugin Name ===
Contributors: ljupcop, gfrenoy
Tags: tabt, sport, table tennis, ranking, result, rank, rating, list, rankings, darts, members, member, api, league, league management, match, electronic darts
Requires at least: 4.5
Tested up to: 4.7.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Displays members, matches and rankings from any TabT compatible application (using TabT-API).

== Description ==

TabT4wp integrates any data managed by a [TabT](http://tabt.frenoy.net) application into your WordPress website.

TabT is a complete and comprehensive suite of applications to manage a sport federation.  It includes many features like
manage members, competitions, rankings.  Most of the data is available through an API that allows creative webmasters
to create new applications or website based on the federation data.

Through the TabT API, TabT4wp creates simple yet powerful shortcodes to help webmasters to display member list, rankings
or other useful information for their website.

TabT4wp has initially been developed by Ljupco Prchkovski and is currently maintained by [frenoy.net](http://www.frenoy.net).

Any question or support request should be sent to [tabt@frenoy.net](mailto:tabt@frenoy.net).

== Installation ==

1. Upload `tabt4wp.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to Settings > TabT Settings and configure your credentials
4. Select the information you want to see for each API table
5. Use any TabT4wp shortcode on your pages

== Available short codes ==

= Seasons =

Display the list of available seasons.

example: [TabT get=Seasons]

= Clubs =

Display the list of clubs for a given season.  If no season is given, the last season is used.

example: [TabT get=Clubs season=17]

= ClubDetails =

Display details about a club.

= Divisions =

Display the list available divisions.

= DivisionRanking =

Display division ranking.

example: [TabT get="DivisionRanking" Season="17" DivisionId="2960"]

= Members =

Display the list of members.

= MemberDetails =

Display information about a member.

= Matches =

Display a list of matches.

= MatchDetails =

Display details of a match.

== Frequently Asked Questions ==

= What is TabT ? =

[TabT](http://tabt.frenoy.net) is a complete and comprehensive suite of applications to manage a sport federation.  It includes
many features like manage members, competitions, rankings.

= Who is using TabT ? =

- [Vlaamse Tafeltennisliga](http://www.vttl.be) - http://competitie.vttl.be
- [Sporta](http://www.sportafederatie.be/) - https://ttonline.sporta.be
- [Koninklijke Antwerpse Vereniging van Vriendenclubs](http://www.kavvv.be) - https://tafeltennis.kavvv.be
- [Electronic Darts Belgium](http://www.edb-darts.be/) - https://competitie.edb-darts.be

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==

None (yet ;-p)
