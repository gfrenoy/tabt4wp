<?php

/**
 * @link              https://bitbucket.org/gfrenoy/tabt4wp
 * @since             1.0.0
 * @package           Tabt4wp
 *
 * @wordpress-plugin
 * Plugin Name:       TabT4WP
 * Plugin URI:        https://bitbucket.org/gfrenoy/tabt4wp
 * Description:       Displays members, matches and rankings from any TabT compatible application (using TabT-API).
 * Version:           1.0.0
 * Author:            Ljupcho Prchkovski
 * Author URI:        https://bitbucket.org/gfrenoy/tabt4wp
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tabt4wp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
  die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tabt4wp-activator.php
 */
function activate_tabt4wp() {
  require_once plugin_dir_path( __FILE__ ) . 'includes/class-tabt4wp-activator.php';
  Tabt4wp_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tabt4wp-deactivator.php
 */
function deactivate_tabt4wp() {
  require_once plugin_dir_path( __FILE__ ) . 'includes/class-tabt4wp-deactivator.php';
  Tabt4wp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tabt4wp' );
register_deactivation_hook( __FILE__, 'deactivate_tabt4wp' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tabt4wp.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tabt4wp() {
  $plugin = new Tabt4wp();
  $plugin->run();

}
run_tabt4wp();
