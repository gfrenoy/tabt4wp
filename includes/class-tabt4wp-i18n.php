<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 *
 * @package    Tabt4wp
 * @subpackage Tabt4wp/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Tabt4wp
 * @subpackage Tabt4wp/includes
 * @author     Ljupcho Prchkovski <ljupcho.prchkovski@gmail.com>
 */
class Tabt4wp_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		
		load_plugin_textdomain(
			'tabt4wp',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
