<?php
/**
 *  This file is part of TabT4wp
 *
 *  Copyright (C) 2017, Ljupco Prchkovski
 *  Copyright (C) 2017, frenoy.net sprl (info@frenoy.net)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class TabT_List_Table extends WP_List_Table {
  private $order;
  private $orderby;
  private $posts_per_page = 5;
  private $tabledata;

  public function __construct() {
    parent :: __construct(
      array(
        'singular' => 'tabttable',
        'plural' => 'tabttable',
        'ajax' => true,
        'screen' => '-',
      )
    );
  }

  public function no_items() {
    _e( 'No data.' );
  }

  /**
   * @see WP_List_Table::get_views()
   */
  public function get_views() {
    return array();
  } 

  /**
   * @see WP_List_Table::get_columns()
   */
  public function get_columns() {
    return array();
  }

  /**
   * Prepare data for display
   * @see WP_List_Table::prepare_items()
   */
  function prepare_items() {
    $this->_column_headers = array($this->tabledata['columns'], $this->tabledata['hidden'], $this->tabledata['sortable']);
    $this->items = $this->tabledata['data'];
  }

  protected function get_sortable_columns() {
    return $this->tabledata['sortable'];
  }

  function prepare_tabledata($tabledata) {
    $this->tabledata = $tabledata;
  }

  function column_default( $item, $column_name ) {
    return $item[ $column_name ];
  }


  /**
   * Override of table nav to avoid breaking with bulk actions & according nonce field
   */
  public function display_tablenav( $which ) {
    ?>
    <div class="tablenav <?php echo esc_attr( $which ); ?>">
    <?php
    $this->extra_tablenav( $which );
    $this->pagination( $which );
    ?>
    <br class="clear" />
    </div>
    <?php
  }

  /**
   * Disables the views for 'side' context as there's not enough free space in the UI
   * Only displays them on screen/browser refresh. Else we'd have to do this via an AJAX DB update.
   * 
   * @see WP_List_Table::extra_tablenav()
   */
  public function extra_tablenav( $which )
  {
    global $wp_meta_boxes;
    $views = $this->get_views();
    if ( empty( $views ) ) {
      return;
    }

    $this->views();
  }

  public function display() {
    ?>
    <table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
    <thead>
    <tr>
    <?php $this->print_column_headers(); ?>
    </tr>
    </thead>

    <tbody id="the-list" data-wp-lists="list:tableexample">
    <?php $this->display_rows_or_placeholder(); ?>
    </tbody>

    </table>
    <?php
    $this->display_tablenav( 'bottom' );
  }


  protected function handle_row_actions( $item, $column_name, $primary ) {
    return '';
  }
}
