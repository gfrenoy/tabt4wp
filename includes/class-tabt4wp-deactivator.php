<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 *
 * @package    Tabt4wp
 * @subpackage Tabt4wp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tabt4wp
 * @subpackage Tabt4wp/includes
 * @author     Ljupcho Prchkovski <ljupcho.prchkovski@gmail.com>
 */
class Tabt4wp_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
