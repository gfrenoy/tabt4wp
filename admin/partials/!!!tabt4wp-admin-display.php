<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://bitbucket.org/gfrenoy/tabt4wp
 * @since      1.0.0
 *
 * @package    Tabt4wp
 * @subpackage Tabt4wp/admin/partials
 */
?>

<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <form action="options.php" method="post">
        <?php
            settings_fields( $this->plugin_name );
            do_settings_sections( $this->plugin_name );
            submit_button();

		/*$server_url = $value = get_option( 'tabt4wp_server_url' );
		$server_account = $value = get_option( 'tabt4wp_server_account' );
		$server_password = $value = get_option( 'tabt4wp_server_password' );
    $creds = array ( 'Account' => $server_account, 'Password' => $server_password );
    $s = new SoapClient ( $server_url, array ( 'trace' => true ) );
    $resp = $s->GetClubs( array ( 'Credentials' => $creds, 'Season' => 17 ));
		echo '<pre>';
		var_dump($resp);
		echo '</pre>';*/
        ?>
    </form>
    
<?php     
?>
</div>