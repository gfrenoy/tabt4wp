<?php

/**
* The admin-specific functionality of the plugin.
*
* @link       https://bitbucket.org/gfrenoy/tabt4wp
* @since      1.0.0
*
* @package    Tabt4wp
* @subpackage Tabt4wp/admin
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @package    Tabt4wp
* @subpackage Tabt4wp/admin
* @author     Ljupcho Prchkovski <ljupcho.prchkovski@gmail.com>
*/
class Tabt4wp_Admin
{

	private $option_name = 'tabt4wp';
	private $plugin_name;
	private $version;
	private $dir;
	private $file;
	private $assets_dir;
	private $assets_url;
	private $settings;
	private $l;

	/**
	* Initialize the class and set its properties.
	*
	* @since    1.0.0
	* @param      string    $plugin_name       The name of this plugin.
	* @param      string    $version    The version of this plugin.
	*/
	public function __construct( $plugin_name, $version, $labels )
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->l = $labels;
		$this->dir = dirname( $plugin_name );
		$this->assets_dir = trailingslashit( $this->dir ) . 'assets';
		$this->assets_url = esc_url( trailingslashit( plugins_url( '/assets/', $plugin_name ) ) );
		$this->settings_base = 'tabt4wp_';
	}

	public function enqueue_styles()
	{
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tabt4wp-admin.css', array(), $this->version, 'all' );
	}

	public function enqueue_scripts()
	{
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tabt4wp-admin.js', array('jquery' ), $this->version, false );
	}

	public function add_options_page()
	{

		/*$this->plugin_screen_hook_suffix = add_options_page(
		__( 'TabT4WP Settings', 'tabt4wp' ),
		__( 'TabT4WP', 'tabt4wp' ),
		'manage_options',
		$this->plugin_name,
		array( $this, 'display_options_page' )
		);*/

	}

	public function display_options_page()
	{
		include_once 'partials/tabt4wp-admin-display.php';
	}


	public function init()
	{
		$this->settings = $this->settings_fields();
	}
	/**
	* Add settings page to admin menu
	* @return void
	*/
	public function add_menu_item()
	{
		$page = add_options_page( __( 'TabT Settings', 'tabt4wp' ) , __( 'TabT Settings', 'tabt4wp' ) , 'manage_options' , $this->plugin_name ,  array($this,'settings_page' ) );
	}

	public function add_settings_link( $links )
	{
		$settings_link = '<a href="options-general.php?page=tabt4wp">' . __( 'Settings', $this->plugin_name ) . '</a>';
		array_push( $links, $settings_link );
		return $links;
	}

	public function clean_cache()
	{
		if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true') {
			global $wpdb;
			$wpdb->query("DELETE FROM $wpdb->options WHERE $wpdb->options.option_name LIKE '_transient_TabT_%' OR $wpdb->options.option_name LIKE '_transient_timeout_TabT_%'");
		}
	}

	private function settings_fields()
	{
		$settings['API'] = array(
			'title'      => __( 'TabT API', $this->plugin_name ),
			'description'=> __( 'API credentials used to access to TabT API server.', $this->plugin_name ),
			'fields'     				=> array(
				array(
					'id'         => 'tabt_url',
					'label'      => __( 'TabT Server' , $this->plugin_name ),
					'description'=> __( 'Server URL.', $this->plugin_name ),
					'type'       => 'text',
					'default'    => '',
					'placeholder'=> __( 'https://domain', $this->plugin_name )
				),
				array(
					'id'         => 'server_url',
					'label'      => __( 'TabT API Server' , $this->plugin_name ),
					'description'=> __( 'Server URL where TabT API can be accessed.', $this->plugin_name ),
					'type'       => 'text',
					'default'    => '',
					'placeholder'=> __( 'https://domain/api/?wsdl', $this->plugin_name )
				),
				array(
					'id'         => 'server_account',
					'label'      => __( 'Username' , $this->plugin_name ),
					'description'=> __( 'API account username.', $this->plugin_name ),
					'type'       => 'text',
					'default'    => '',
					'placeholder'=> __( 'username', $this->plugin_name )
				),
				array(
					'id'         => 'server_password',
					'label'      => __( 'Password' , $this->plugin_name ),
					'description'=> __( 'API account password.', $this->plugin_name ),
					'type'       => 'password',
					'default'    => '',
					'placeholder'=> __( 'password', $this->plugin_name )
				),
				array(
					'id'         => 'sepaprator',
					'label'      => __( 'Sepparator' , $this->plugin_name ),
					'description'=> __( 'Results sepparator.', $this->plugin_name ),
					'type'       => 'text',
					'default'    => ':',
					'placeholder'=> __( ':', $this->plugin_name )
				),
				array(
					'id'         => 'data_cache',
					'label'      => __( 'API Data Cache', $this->plugin_name ),
					'description'=> __( 'Make your pages loading faster, save your bandwith and keep safe from API limits using cached data.<br />Select how much hours to use cached data until next real data fetching.', $this->plugin_name ),
					'type'       => 'radio',
					'options'    => array('cache0' => 'Do not use cache','cache1' => 'Each hour','cache3' => 'Each 3 hours','cache6' => 'Each 6 hours','cache12'=> 'Each 12 hours','cache24'=> 'Each 24 hours' ),
					'default'    => 'cache6'
				)
			)
		);
		$settings['extra'] = array(
			'title'      => __( 'API Tables', $this->plugin_name ),
			'description'=> __( 'Define tables columns visibility per table type.<br />Select one or more columns to be displayed in each table.', $this->plugin_name ),
			'fields'     				=> array(
				array(
					'id'         => 'table_Clubs',
					'label'      => __( 'Clubs', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Clubs_ClubCount'   => $this->l['Clubs_ClubCount'],
						'Clubs_UniqueIndex' => $this->l['Clubs_UniqueIndex'],
						'Clubs_Name'        => $this->l['Clubs_Name'],
						'Clubs_LongName'    => $this->l['Clubs_LongName'],
						'Clubs_Category'    => $this->l['Clubs_Category'],
						'Clubs_CategoryName'=> $this->l['Clubs_CategoryName'],
						'Clubs_Actions'		=> $this->l['Clubs_Actions'],
					),
					'default'    => array('Clubs_ClubCount','Clubs_Name','Clubs_CategoryName' )
				),
				array(
					'id'         => 'table_ClubTeams',
					'label'      => __( 'Club Teams', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'ClubTeams_ClubName'        => $this->l['ClubTeams_ClubName'],
						'ClubTeams_TeamCount'       => $this->l['ClubTeams_TeamCount'],
						'ClubTeams_TeamId'          => $this->l['ClubTeams_TeamId'],
						'ClubTeams_Team'            => $this->l['ClubTeams_Team'],
						'ClubTeams_DivisionId'      => $this->l['ClubTeams_DivisionId'],
						'ClubTeams_DivisionName'    => $this->l['ClubTeams_DivisionName'],
						'ClubTeams_DivisionCategory'=> $this->l['ClubTeams_DivisionCategory'],
						'ClubTeams_MatchType'       => $this->l['ClubTeams_MatchType'],
					),
					'default'    => array()
				),
				array(
					'id'         => 'table_Divisions',
					'label'      => __( 'Divisions', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Divisions_DivisionCount'=> $this->l['Divisions_DivisionCount'],
						'Divisions_DivisionId'=> $this->l['Divisions_DivisionId'],
						'Divisions_DivisionName'=> $this->l['Divisions_DivisionName'],
						'Divisions_DivisionCategory'=> $this->l['Divisions_DivisionCategory'],
						'Divisions_Level'=> $this->l['Divisions_Level'],
						'Divisions_MatchType'=> $this->l['Divisions_MatchType'],
						'Divisions_Actions'=> $this->l['Divisions_Actions'],
					),
					'default'    => array()
				),
				array(
					'id'         => 'table_DivisionRanking',
					'label'      => __( 'Division Ranking', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'DivisionRanking_DivisionName'         => $this->l['DivisionRanking_DivisionName'],
						'DivisionRanking_Position'             => $this->l['DivisionRanking_Position'],
						'DivisionRanking_Team'                 => $this->l['DivisionRanking_Team'],
						'DivisionRanking_GamesPlayed'          => $this->l['DivisionRanking_GamesPlayed'],
						'DivisionRanking_GamesWon'             => $this->l['DivisionRanking_GamesWon'],
						'DivisionRanking_GamesLost'            => $this->l['DivisionRanking_GamesLost'],
						'DivisionRanking_GamesDraw'            => $this->l['DivisionRanking_GamesDraw'],
						'DivisionRanking_IndividualMatchesWon' => $this->l['DivisionRanking_IndividualMatchesWon'],
						'DivisionRanking_IndividualMatchesLost'=> $this->l['DivisionRanking_IndividualMatchesLost'],
						'DivisionRanking_IndividualSetsWon'    => $this->l['DivisionRanking_IndividualSetsWon'],
						'DivisionRanking_IndividualSetsLost'   => $this->l['DivisionRanking_IndividualSetsLost'],
						'DivisionRanking_Points'               => $this->l['DivisionRanking_Points'],
						'DivisionRanking_TeamClub'             => $this->l['DivisionRanking_TeamClub'],
					),
					'default'    => array()
				),
				array(
					'id'         => 'table_Matches',
					'label'      => __( 'Matches', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Matches_MatchCount'      => $this->l['Matches_MatchCount'],
						'Matches_DivisionName'    => $this->l['Matches_DivisionName'],
						'Matches_MatchId'         => $this->l['Matches_MatchId'],
						'Matches_WeekName'        => $this->l['Matches_WeekName'],
						'Matches_Date'            => $this->l['Matches_Date'],
						'Matches_Time'            => $this->l['Matches_Time'],
						'Matches_Venue'           => $this->l['Matches_Venue'],
						'Matches_HomeClub'        => $this->l['Matches_HomeClub'],
						'Matches_HomeTeam'        => $this->l['Matches_HomeTeam'],
						'Matches_AwayClub'        => $this->l['Matches_AwayClub'],
						'Matches_AwayTeam'        => $this->l['Matches_AwayTeam'],
						'Matches_Score'           => $this->l['Matches_Score'],
						'Matches_MatchUniqueId'   => $this->l['Matches_MatchUniqueId'],
						'Matches_NextWeekName'    => $this->l['Matches_NextWeekName'],
						'Matches_PreviousWeekName'=> $this->l['Matches_PreviousWeekName'],
						'Matches_IsHomeForfeited' => $this->l['Matches_IsHomeForfeited'],
						'Matches_IsAwayForfeited' => $this->l['Matches_IsAwayForfeited'],
						'Matches_MatchDetails'    => $this->l['Matches_MatchDetails'],
						'Matches_Actions'    	  => $this->l['Matches_Actions'],
					),
					'default'    => array()
				),
				array(
					'id'         => 'table_Members',
					'label'      => __( 'Members', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Members_MemberCount'        => $this->l['Members_MemberCount'],
						'Members_Position'            => $this->l['Members_Position'],
						'Members_UniqueIndex'         => $this->l['Members_UniqueIndex'],
						'Members_RankingIndex'        => $this->l['Members_RankingIndex'],
						'Members_FirstName'           => $this->l['Members_FirstName'],
						'Members_LastName'            => $this->l['Members_LastName'],
						'Members_Ranking'             => $this->l['Members_Ranking'],
						'Members_Actions'       	  => $this->l['Members_Actions'],
					),
					'default'   => array()
				),
				/*array(
					'id'         => 'table_MemberDetails',
					'label'      => __( 'Member Details', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'MemberDetails_Position'            => $this->l['Members_Position'],
						'MemberDetails_UniqueIndex'         => $this->l['MemberDetails_UniqueIndex'],
						'MemberDetails_FirstName'           => $this->l['MemberDetails_FirstName'],
						'MemberDetails_LastName'            => $this->l['MemberDetails_LastName'],
						'MemberDetails_Ranking'             => $this->l['MemberDetails_Ranking'],
						'MemberDetails_Status'              => $this->l['MemberDetails_Status'],
						'MemberDetails_Club'                => $this->l['MemberDetails_Club'],
						'MemberDetails_Gender'              => $this->l['MemberDetails_Gender'],
						'MemberDetails_Category'            => $this->l['MemberDetails_Category'],
						'MemberDetails_BirthDate'           => $this->l['MemberDetails_BirthDate'],
						'MemberDetails_Email'               => $this->l['MemberDetails_Email'],
					),
					'default'   => array()
				),*/
				array(
					'id'         => 'table_Seasons',
					'label'      => __( 'Seasons', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Seasons_CurrentSeasonName'=> $this->l['Seasons_CurrentSeasonName'],
						'Seasons_Season'       => $this->l['Seasons_Season'],
						'Seasons_Name'         => $this->l['Seasons_Name'],
						'Seasons_IsCurrent'    => $this->l['Seasons_IsCurrent'],
						'Seasons_Actions'    	=> $this->l['Seasons_Actions'],
					),
					'default'    => array('Season_CurrentSeason','Seasons_Name' )
				),
				array(
					'id'         => 'table_Tournaments',
					'label'      => __( 'Tournaments', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'Tournaments_Columns'=> $this->l['Tournaments_Columns'],
					),
					'default'    => array()
				),
				/*array(
					'id'         => 'table_MatchSystems',
					'label'      => __( 'Match Systems', $this->plugin_name ),
					'description'=> __( '', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    => array(
						'MatchSystems_Columns'=> $this->l['MatchSystems_Columns'],
					),
					'default'    => array()
				),*/
			)
		);
		$settings = apply_filters( 'tabt4wp_fields', $settings );
		return $settings;
	}

	private function settings_fields_all_types()
	{
		$settings['standard'] = array(
			'title'      => __( 'API', $this->plugin_name ),
			'description'=> __( 'These are fairly standard form input fields.', $this->plugin_name ),
			'fields'     				=> array(
				array(
					'id'         => 'text_field',
					'label'      => __( 'Some Text' , $this->plugin_name ),
					'description'=> __( 'This is a standard text field.', $this->plugin_name ),
					'type'       => 'text',
					'default'    => '',
					'placeholder'=> __( 'Placeholder text', $this->plugin_name )
				),
				array(
					'id'         => 'password_field',
					'label'      => __( 'A Password' , $this->plugin_name ),
					'description'=> __( 'This is a standard password field.', $this->plugin_name ),
					'type'       => 'password',
					'default'    => '',
					'placeholder'=> __( 'Placeholder text', $this->plugin_name )
				),
				array(
					'id'         => 'secret_text_field',
					'label'      => __( 'Some Secret Text' , $this->plugin_name ),
					'description'=> __( 'This is a secret text field - any data saved here will not be displayed after the page has reloaded, but it will be saved.', $this->plugin_name ),
					'type'       => 'text_secret',
					'default'    => '',
					'placeholder'=> __( 'Placeholder text', $this->plugin_name )
				),
				array(
					'id'         => 'text_block',
					'label'      => __( 'A Text Block' , $this->plugin_name ),
					'description'=> __( 'This is a standard text area.', $this->plugin_name ),
					'type'       => 'textarea',
					'default'    => '',
					'placeholder'=> __( 'Placeholder text for this textarea', $this->plugin_name )
				),
				array(
					'id'         => 'single_checkbox',
					'label'      => __( 'An Option', $this->plugin_name ),
					'description'=> __( 'A standard checkbox - if you save this option as checked then it will store the option as \'on\', otherwise it will be an empty string.', $this->plugin_name ),
					'type'       => 'checkbox',
					'default'    => ''
				),
				array(
					'id'         => 'select_box',
					'label'      => __( 'A Select Box', $this->plugin_name ),
					'description'=> __( 'A standard select box.', $this->plugin_name ),
					'type'       => 'select',
					'options'    		=> array('drupal'   => 'Drupal','joomla'   => 'Joomla','wordpress'=> 'WordPress' ),
					'default'    => 'wordpress'
				),
				array(
					'id'         => 'radio_buttons',
					'label'      => __( 'Some Options', $this->plugin_name ),
					'description'=> __( 'A standard set of radio buttons.', $this->plugin_name ),
					'type'       => 'radio',
					'options'    		=> array('superman'=> 'Superman','batman'  => 'Batman','ironman' => 'Iron Man' ),
					'default'    => 'batman'
				),
				array(
					'id'         => 'multiple_checkboxes',
					'label'      => __( 'Some Items', $this->plugin_name ),
					'description'=> __( 'You can select multiple items and they will be stored as an array.', $this->plugin_name ),
					'type'       => 'checkbox_multi',
					'options'    		=> array('square'   => 'Square','circle'   => 'Circle','rectangle'=> 'Rectangle','triangle' => 'Triangle' ),
					'default'    		=> array('circle','triangle' )
				)
			)
		);
		$settings['extra'] = array(
			'title'      => __( 'Extra', $this->plugin_name ),
			'description'=> __( 'These are some extra input fields that maybe aren\'t as common as the others.', $this->plugin_name ),
			'fields'     				=> array(
				array(
					'id'         => 'number_field',
					'label'      => __( 'A Number' , $this->plugin_name ),
					'description'=> __( 'This is a standard number field - if this field contains anything other than numbers then the form will not be submitted.', $this->plugin_name ),
					'type'       => 'number',
					'default'    => '',
					'placeholder'=> __( '42', $this->plugin_name )
				),
				array(
					'id'         => 'colour_picker',
					'label'      => __( 'Pick a colour', $this->plugin_name ),
					'description'=> __( 'This uses WordPress\' built-in colour picker - the option is stored as the colour\'s hex code.', $this->plugin_name ),
					'type'       => 'color',
					'default'    => '#21759B'
				),
				array(
					'id'         => 'an_image',
					'label'      => __( 'An Image' , $this->plugin_name ),
					'description'=> __( 'This will upload an image to your media library and store the attachment ID in the option field. Once you have uploaded an imge the thumbnail will display above these buttons.', $this->plugin_name ),
					'type'       => 'image',
					'default'    => '',
					'placeholder'=> ''
				),
				array(
					'id'         => 'multi_select_box',
					'label'      => __( 'A Multi-Select Box', $this->plugin_name ),
					'description'=> __( 'A standard multi-select box - the saved data is stored as an array.', $this->plugin_name ),
					'type'       => 'select_multi',
					'options'    		=> array('linux'  => 'Linux','mac'    => 'Mac','windows'=> 'Windows' ),
					'default'    		=> array('linux' )
				)
			)
		);
		$settings = apply_filters( 'tabt4wp_fields', $settings );
		return $settings;
	}


	public function register_settings()
	{



		if ( is_array( $this->settings ) ) {
			foreach ( $this->settings as $section => $data ) {
				// Add section to page
				add_settings_section( $section, $data['title'], array($this,'settings_section' ), 'tabt4wp' );
				foreach ( $data['fields'] as $field ) {
					// Validation callback for field
					$validation = '';
					if ( isset( $field['callback'] ) ) {
						$validation = $field['callback'];
					}
					// Register field
					$option_name = $this->option_name . '_' . $field['id'];
					register_setting( 'tabt4wp', $option_name, $validation );
					// Add field to page
					add_settings_field( $field['id'], $field['label'], array($this,'display_field' ), 'tabt4wp', $section, array('field'=> $field ) );
				}
			}
		}

	}


	public function settings_section( $section )
	{
		$html = '<p>' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
		echo $html;
	}

	public function display_field( $args )
	{
		$field       = $args['field'];
		$html        = '';
		$option_name = $this->settings_base . $field['id'];
		$option      = get_option( $option_name );
		$data        = '';
		if ( isset( $field['default'] ) ) {
			$data = $field['default'];
			if ( $option ) {
				$data = $option;
			}
		}
		switch ( $field['type'] ) {
			case 'text':
			case 'password':
			case 'number':
			$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . $field['type'] . '" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value="' . $data . '"/>' . "\n";
			break;
			case 'text_secret':
			$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="text" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" value=""/>' . "\n";
			break;
			case 'textarea':
			$html .= '<textarea id="' . esc_attr( $field['id'] ) . '" rows="5" cols="50" name="' . esc_attr( $option_name ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '">' . $data . '</textarea><br/>'. "\n";
			break;
			case 'checkbox':
			$checked = '';
			if ( $option && 'on' == $option ) {
				$checked = 'checked="checked"';
			}
			$html .= '<input id="' . esc_attr( $field['id'] ) . '" type="' . $field['type'] . '" name="' . esc_attr( $option_name ) . '" ' . $checked . '/>' . "\n";
			break;
			case 'checkbox_multi':
			foreach ( $field['options'] as $k => $v ) {
				$checked = false;
				if ( in_array( $k, $data ) ) {
					$checked = true;
				}
				$html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '"><input type="checkbox" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '[]" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
			}
			break;
			case 'radio':
			foreach ( $field['options'] as $k => $v ) {
				$checked = false;
				if ( $k == $data ) {
					$checked = true;
				}
				$html .= '<label for="' . esc_attr( $field['id'] . '_' . $k ) . '"><input type="radio" ' . checked( $checked, true, false ) . ' name="' . esc_attr( $option_name ) . '" value="' . esc_attr( $k ) . '" id="' . esc_attr( $field['id'] . '_' . $k ) . '" /> ' . $v . '</label> ';
			}
			break;
			case 'select':
			$html .= '<select name="' . esc_attr( $option_name ) . '" id="' . esc_attr( $field['id'] ) . '">';
			foreach ( $field['options'] as $k => $v ) {
				$selected = false;
				if ( $k == $data ) {
					$selected = true;
				}
				$html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '">' . $v . '</option>';
			}
			$html .= '</select> ';
			break;
			case 'select_multi':
			$html .= '<select name="' . esc_attr( $option_name ) . '[]" id="' . esc_attr( $field['id'] ) . '" multiple="multiple">';
			foreach ( $field['options'] as $k => $v ) {
				$selected = false;
				if ( in_array( $k, $data ) ) {
					$selected = true;
				}
				$html .= '<option ' . selected( $selected, true, false ) . ' value="' . esc_attr( $k ) . '" />' . $v . '</label> ';
			}
			$html .= '</select> ';
			break;
			case 'image':
			$image_thumb = '';
			if ( $data ) {
				$image_thumb = wp_get_attachment_thumb_url( $data );
			}
			$html .= '<img id="' . $option_name . '_preview" class="image_preview" src="' . $image_thumb . '" /><br/>' . "\n";
			$html .= '<input id="' . $option_name . '_button" type="button" data-uploader_title="' . __( 'Upload an image' , $this->plugin_name ) . '" data-uploader_button_text="' . __( 'Use image' , $this->plugin_name ) . '" class="image_upload_button button" value="'. __( 'Upload new image' , $this->plugin_name ) . '" />' . "\n";
			$html .= '<input id="' . $option_name . '_delete" type="button" class="image_delete_button button" value="'. __( 'Remove image' , $this->plugin_name ) . '" />' . "\n";
			$html .= '<input id="' . $option_name . '" class="image_data_field" type="hidden" name="' . $option_name . '" value="' . $data . '"/><br/>' . "\n";
			break;
			case 'color':
			?>
			<div class="color-picker" style="position:relative;">
				<input type="text" name="<?php esc_attr_e( $option_name ); ?>" class="color" value="<?php esc_attr_e( $data ); ?>" />
				<div style="position:absolute;background:#FFF;z-index:99;border-radius:100%;" class="colorpicker">
				</div>
			</div>
			<?php
			break;
		}
		switch ( $field['type'] ) {
			case 'checkbox_multi':
			case 'radio':
			case 'select_multi':
			$html .= '<br/><span class="description">' . $field['description'] . '</span>';
			break;
			default:
			$html .= '<label for="' . esc_attr( $field['id'] ) . '"><span class="description">' . $field['description'] . '</span></label>' . "\n";
			break;
		}
		echo $html;
	}

	public function validate_field( $data )
	{
		if ( $data && strlen( $data ) > 0 && $data != '' ) {
			$data = urlencode( strtolower( str_replace( ' ' , '-' , $data ) ) );
		}
		return $data;
	}

	public function settings_page()
	{
		// Build page HTML
		$html = '<div class="wrap" id="tabt4wp">' . "\n";
		$html .= '<h1>' . __( 'Plugin Settings' , $this->plugin_name ) . '</h1>' . "\n";
		$html .= '<form method="post" action="options.php" enctype="multipart/form-data">' . "\n";
		// Setup navigation
		$html .= '<ul id="settings-sections" class="subsubsub hide-if-no-js">' . "\n";
		$html .= '<li><a class="tab all current" href="#all">' . __( 'All' , $this->plugin_name ) . '</a></li>' . "\n";
		foreach ( $this->settings as $section => $data ) {
			$html .= '<li>| <a class="tab" href="#' . $section . '">' . $data['title'] . '</a></li>' . "\n";
		}
		$html .= '</ul>' . "\n";
		$html .= '<div class="clear"></div>' . "\n";
		// Get settings fields
		ob_start();
		settings_fields( 'tabt4wp' );
		do_settings_sections( 'tabt4wp' );
		$html .= ob_get_clean();
		$html .= '<p class="submit">' . "\n";
		$html .= '<input name="Submit" type="submit" class="button-primary" value="' . esc_attr( __( 'Save Settings' , $this->plugin_name ) ) . '" />' . "\n";
		$html .= '</p>' . "\n";
		$html .= '</form>' . "\n";
		$html .= '</div>' . "\n";
		echo $html;
	}

}
